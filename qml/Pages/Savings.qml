/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import "../Components"
import "../js/Database.js" as MyDB
import "../js/Helper.js" as Helper

Page {
  id: pageSavings

  header: BaseHeader {
    title: i18n.tr('Savings')

    flickable: mainFlickable

    trailingActionBar {
       actions: [
         Action {
            iconName: "info"
            text: i18n.tr("About")

            onTriggered: pageStack.push(Qt.resolvedUrl("./About.qml"))
         }
      ]
    }
  }

  ScrollView {
      width: parent.width
      height: parent.height
      contentItem: mainFlickable
  }

  Flickable {
        id: mainFlickable

        width: parent.width
        height: parent.height

        contentHeight: mainColumn.height
        topMargin: units.gu(2)

        Column {
            id: mainColumn
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)
            spacing: units.gu(2)

            Rectangle {
              id: savingsRectable

              anchors {
                left: parent.left
                right: parent.right

                leftMargin: units.gu(2)
                rightMargin: units.gu(2)
              }

              radius: units.gu(1)
              color: settings.cardColor

              height: newSavingTitle.visible ? newSavingTitle.height + newSavingProgress.height + (settings.margin * 3) : settings.margin * 3

              TextField {
                id: newSavingTitle

                visible: !addSavingButton.visible

                anchors {
                  top: parent.top
                  left: parent.left
                  topMargin: settings.margin
                  leftMargin: settings.margin
                }

                placeholderText: i18n.tr('Name')
              }

              TextField {
                id: newSavingAmount

                visible: !addSavingButton.visible

                anchors {
                  top: parent.top
                  right: saveSavingButton.left
                  left: newSavingTitle.right
                  topMargin: settings.margin
                  rightMargin: settings.margin
                  leftMargin: settings.margin
                }

                inputMethodHints: Qt.ImhFormattedNumbersOnly
                hasClearButton: false
                placeholderText: i18n.tr('Amount')
              }

              ProgressBar {
                  id: newSavingProgress

                  visible: !addSavingButton.visible

                  anchors {
                    left: parent.left
                    right: saveSavingButton.left
                    bottom: parent.bottom
                    bottomMargin: settings.margin
                    leftMargin: settings.margin
                    rightMargin: settings.margin
                  }

                  minimumValue: 0
                  maximumValue: 100

                  value: 0
              }

              Button {
                id: saveSavingButton

                visible: !addSavingButton.visible
                enabled: newSavingTitle.text != "" && newSavingAmount.text != "" && newSavingAmount.text > 0

                anchors {
                  right: parent.right
                  top: parent.top
                  bottom: parent.bottom
                  rightMargin: settings.margin
                  bottomMargin: settings.margin
                  topMargin: settings.margin
                }

                width: units.gu(4)

                iconName: "ok"
                color: "transparent"

                onClicked: {
                  Helper.createSaving();
                  addSavingButton.visible = true
                }
              }

              Button {
                id: addSavingButton

                visible: true

                anchors.fill: parent

                color: "transparent"

                Icon {
                    anchors.centerIn: parent
                    width: units.gu(3)
                    height: units.gu(3)
                    name: "add"
                }

                onClicked: addSavingButton.visible = false
              }
            }
        }
    }

    Component.onCompleted: {
      Helper.createAllSavingObjects();
    }
}
