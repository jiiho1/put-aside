import QtQuick 2.9
import Lomiri.Components 1.3

Rectangle {
  id: savingsRectable

  property int id: -1
  property string title: "[Title]"
  property double amount: 0.00
  property int percent: 0

  property var progressObject: null
  property var amountObject: null

  anchors {
    left: parent.left
    right: parent.right
    leftMargin: units.gu(2)
    rightMargin: units.gu(2)
  }

  radius: units.gu(1)
  color: settings.cardColor

  height: savingsTitle.height + savingsProgress.height + (settings.margin * 3)

  Label {
    id: savingsTitle

    anchors {
      top: parent.top
      left: parent.left
      right: savingsAmount.left
      topMargin: settings.margin
      leftMargin: settings.margin
      rightMargin: settings.margin
    }

    text: title
    wrapMode: Text.Wrap
    textSize: Label.Large
//     color: settings.foregroundColor
  }

  Label {
    id: savingsAmount

    anchors {
      top: parent.top
      right: goToDetailsButton.left
      topMargin: settings.margin
      rightMargin: settings.margin
    }

    text: amount.toFixed(2)
    wrapMode: Text.Wrap
    textSize: Label.Large
//     color: settings.foregroundColor

    Component.onCompleted: amountObject = this
  }

  ProgressBar {
      id: savingsProgress

      anchors {
        left: parent.left
        right: goToDetailsButton.left
        bottom: parent.bottom
        bottomMargin: settings.margin
        leftMargin: settings.margin
        rightMargin: settings.margin
      }

      minimumValue: 0
      maximumValue: 100

      value: percent

      Component.onCompleted: progressObject = this
  }

  Button {
    id: goToDetailsButton

    anchors {
      right: parent.right
      top: parent.top
      bottom: parent.bottom
      rightMargin: settings.margin
      bottomMargin: settings.margin
      topMargin: settings.margin
    }

    width: units.gu(4)

    color: "transparent"

    Icon {
        anchors.centerIn: parent
        width: units.gu(3)
        height: units.gu(3)
        name: "go-next"
    }

    onClicked: pageStack.push(Qt.resolvedUrl("../Pages/SavingDetails.qml"), {id: id, rectangleObject: this.parent, progressObject: progressObject, amountObject: amountObject})
  }
}
