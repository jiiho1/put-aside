import QtQuick 2.9
import Lomiri.Components 1.3

PageHeader {
  id: baseHeader

  // Keeping this file for custom themes later. It is currently not needed for the system theme.

//   StyleHints {
//     foregroundColor: settings.foregroundColor
//     backgroundColor: settings.backgroundColor
//     dividerColor: settings.backgroundColor
//   }
}
