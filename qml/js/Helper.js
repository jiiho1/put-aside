var component;

function createSaving() {
  try {
    MyDB.writeSaving(newSavingTitle.text, newSavingAmount.text);
  } catch (err) {
    console.log("Error while saving: " + err);
  } finally {
    createSavingObject(MyDB.readLastSavingId(), newSavingTitle.text, newSavingAmount.text, 0);
    newSavingTitle.text = ""
    newSavingAmount.text = ""
  }
}

function createAllSavingObjects() {
  let databaseObjects = MyDB.readAllSavings();

  for (var i = 0; i < databaseObjects.rows.length; i++) {
    let id = databaseObjects.rows.item(i).id
    let title = databaseObjects.rows.item(i).title
    let amount = databaseObjects.rows.item(i).amount

    createSavingObject(id, title, amount, getSavingProgress(id, amount));
  }
}

function createSavingObject(id, title, amount, percent) {
  component = Qt.createComponent("../Components/SavingsRectangle.qml");

  let saving = component.createObject(mainColumn, {x: 100, y: 100, id: id, title: title, amount: amount, percent});

  if (saving == null) {
      // Error Handling
      console.log("Error creating object");
  }
}

function getSavingProgress(id, amount) {
  let history = MyDB.readSavingHistory(id);

  let saved = 0;

  for (var i = 0; i < history.rows.length; i++) {
    saved += history.rows.item(i).amount
  }

  let progress = saved * 100 / amount;

  return progress;
}

function getHistory(id) {
  let history = MyDB.readSavingHistory(id);
  let saved = 0;

  for (var i = 0; i < history.rows.length; i++) {
    saved += history.rows.item(i).amount;
    historyListModel.append({value: history.rows.item(i).amount.toFixed(2), date: history.rows.item(i).date})
  }

  return saved;
}
